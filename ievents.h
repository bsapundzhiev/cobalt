#ifndef _IEvents_H
#define _IEvents_H
/*
 * Controller events 
 */
class IEvents {

public:
    virtual bool begin() = 0;
    virtual void onKeyDown(unsigned char key, unsigned int time) = 0;
    virtual void onDataAvailable() = 0;
    virtual void onVccMesure(unsigned long vcc) = 0;
};

#endif
