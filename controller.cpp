#include "controller.h"
#include "buttons.h"
#include "lib/PCD8544/PCD8544.h"
extern PCD8544 lcd;

Controller::Controller():
    _views { &_viewSummary, &_viewAltitude, &_viewTemp, &_viewHumidty },
    _currentViewId(0),
    _displayOn(true)
{ 
}

bool Controller::begin() 
{
    for(byte i=0; i < ARR_SIZE(_views); i++) {
        _views[i]->setModel(_model);
    }
    
    bool res = _model.begin();
    showAlert("Initialize.", 2000);
    return res;
}

IView * Controller::getCurrentView()
{
  return _views[_currentViewId];
}

void Controller::onDataAvailable()
{
    _model.readData();
    getCurrentView()->draw();
}

void Controller::onVccMesure(unsigned long vcc)
{   // Calculate percentage
    vcc = vcc - 1800; // subtract 1.9V from vcc, as this is the lowest voltage we will operate at 
    long percent = vcc / 14.0;
    Serial.print("VCC: ");
    Serial.println(percent);
}

void Controller::changeView() 
{ 
    _currentViewId = (++_currentViewId < ARR_SIZE(_views)) ? _currentViewId : 0;    
    getCurrentView()->draw(true);
}

void Controller::onKeyDown(unsigned char key, unsigned int time)
{
    Serial.print("Button ");
    Serial.println(key);
    Serial.print("T: ");
    Serial.println(time);
    beep();
    if (getCurrentView()->onKeyDown(key)) {
      Serial.println("Handle key in view");
      return;
    }
    
    switch(key) {
      case BTN_1:
          changeView();
        break;
        
      case BTN_2:
       
        break;
      case BTN_3:
        
        break;
      case BTN_4:
        switchDisplay();
        break;
      default:
        break;
    }
}

void Controller::showAlert(char *text, unsigned int timeout)
{
    lcd.plotRect(6, 8, LCD_WIDTH-8, LCD_HEIGHT-10);
    lcd.gotoXY(10,2);
    lcd.print(text);
    if(timeout) {
      //delay(timeout);
      timeout = timeout < 1000 ? 1000 : timeout;
      for(unsigned int i = timeout / 1000; i > 0; i-- ) {
        lcd.gotoXY(15, 3);
        lcd.print(i);
        delay(1000);  
      } 
      getCurrentView()->draw(true);
    }
}

void Controller::dislpayTimeout(bool on)
{
    _displayOn = on;
    lcd.setPower(on);
}

void Controller::switchDisplay()
{
    _displayOn = !_displayOn;
    dislpayTimeout(_displayOn);
}

void Controller::highlightDisplay()
{
    //TODO:
}

void Controller::beep() 
{
    //TODO:  
}
