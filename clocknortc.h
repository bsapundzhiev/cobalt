#ifndef CLOCKNORTC_H
#define CLOCKNORTC_H

#define     sec2hour(s)  ((s / 3600) % 24)
#define     sec2min(s)   ((s / 60) % 60) 

class ClockNoRTC
{
    byte _hours;
    byte _mins;
    byte _secs;

public:
    ClockNoRTC():_hours(0), _mins(0),_secs(0) {}
    ~ClockNoRTC() {}
   
    void setTime(unsigned long seconds)
    {
      _hours = sec2hour(seconds);
      _mins = sec2min(seconds);
      _secs = seconds % 60;
    }
    
    unsigned long seconds()
    {
        unsigned long s = _hours * 3600;
        s = s + (_mins * 60);
        s = s + _secs;
        s = s + (millis() / 1000);
        return s;
    }
    
    byte hours()
    {
        unsigned long s = seconds();
        return sec2hour(s);
    }

    byte mins()
    {
        unsigned long s = seconds();
        return sec2min(s);
    }

    byte secs()
    {
        unsigned long sec = seconds();
        sec = sec % 60;
        return sec;
    }
};

#endif
