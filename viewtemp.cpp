#include "viewtemp.h"
#include "lib/PCD8544/PCD8544.h"
extern PCD8544 lcd;

ViewTemp::ViewTemp()
{

}

void ViewTemp::draw(bool force)
{
    if(force) {
      lcd.LcdClear();
    }
    
    lcd.gotoXY(0, 0);
    lcd.print("Hello world");
    
    lcd.plotLine(0,  9, LCD_WIDTH-1, 9);
    
    lcd.gotoXY(1, 2);
    lcd.setLargeNumbers(true);
    lcd.print(_model->temperature, 1);
    lcd.setLargeNumbers(false);
    lcd.print("C");
    
    lcd.plotLine(0, 38 , LCD_WIDTH-1, 38);
    
    lcd.gotoXY(0, 5);
    byte minSymbol[] = MIN_SYMBOL, maxSymbol[] = MAX_SYMBOL;
    lcd.writeBitmap(minSymbol, sizeof(minSymbol));
    lcd.print(_model->getTemperatureExtremum(true), 1);
    
    lcd.gotoXY(LCD_WIDTH / 2, 5);
    lcd.writeBitmap(maxSymbol, sizeof(maxSymbol));
    lcd.print(_model->getTemperatureExtremum(false), 1);
}

bool ViewTemp::onKeyDown(unsigned char key)
{
  return false; 
}
