#include "viewsummary.h"
#include "lib/PCD8544/PCD8544.h"
#include "clocknortc.h"

extern PCD8544 lcd;
extern ClockNoRTC clock;

ViewSummary::ViewSummary()
{

}

void ViewSummary::printWeather()
{
    const char * const weather[] = { "stable ", "sunny  ", "cloudy ", "unstable", "thunder", "unknown" };
    byte index =  _model->getWeatherForecast();
    index = (index > UNKNOWN) ? UNKNOWN : index;
    lcd.print(weather[index]);
}

void ViewSummary::printClock()
{
    if(clock.hours() < 10) {
      lcd.print("0");
    }
    lcd.print(clock.hours());
    lcd.print(":");
    if(clock.mins() < 10) {
      lcd.print("0");
    }
    lcd.print(clock.mins());
}

void ViewSummary::draw(bool force)
{
    if(force) {
      lcd.LcdClear();
    }

    lcd.gotoXY(0, 0);
    printWeather();

    lcd.gotoXY( 84 - 32, 0);
    printClock();

    lcd.plotLine(0,  9, LCD_WIDTH-1, 9);

    lcd.gotoXY(4, 2);
    lcd.setLargeNumbers(true);
    if(_model->getAltitude() < 1000.F) {
        lcd.gotoXY(8, 2);
    }
    lcd.print(_model->getAltitude(), 1);
    lcd.setLargeNumbers(false);
    lcd.print("m");

    lcd.plotLine(0, 38 , LCD_WIDTH-1, 38);

    lcd.gotoXY(0, 5);
    lcd.print("D+:");
    lcd.print(_model->positiveDenivelation, 0);

    //vspeed
    lcd.gotoXY(LCD_WIDTH - 34, 5);
    lcd.print("Vy:");
    lcd.print(_model->verticalSpeed(), 0);
}

bool ViewSummary::onKeyDown(unsigned char key)
{
  return false;
}
