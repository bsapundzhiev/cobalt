#include "iview.h"
#include "model.h"

class ViewTemp: public IView {

  BME280_data * _model;

public:
    ViewTemp();
    virtual void draw(bool force);
    virtual bool onKeyDown(unsigned char key);
    void setModel(BME280_data &model)
    {
        _model = &model;
    };
};
