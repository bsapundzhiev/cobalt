#include "iview.h"
#include "model.h"

class ViewSummary: public IView {

  BME280_data * _model;
  void printWeather();
  void printClock();

public:
    ViewSummary();
    virtual void draw(bool force);
    virtual bool onKeyDown(unsigned char key);
    void setModel(BME280_data &model)
    {
        _model = &model;
    };
};
