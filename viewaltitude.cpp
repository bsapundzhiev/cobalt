#include "viewaltitude.h"
#include "lib/PCD8544/PCD8544.h"
extern PCD8544 lcd;

ViewAltitude::ViewAltitude()
{

}

void ViewAltitude::draw(bool force)
{
    if (force) {
      lcd.LcdClear();
    }
    
    lcd.setLargeNumbers(false);
    lcd.gotoXY(0, 0);
    lcd.print("Hello world");
    lcd.plotLine(0,  9, LCD_WIDTH-1, 9);

    lcd.gotoXY(1, 2);
    lcd.setLargeNumbers(true);
    lcd.print(_model->getPressure() / 100.0F, 1);
    lcd.setLargeNumbers(false);
    lcd.print("hPa");  
    lcd.plotLine(0, 38 , LCD_WIDTH-1, 38);
     
    lcd.gotoXY(0, 5);
   
    byte minSymbol[] = MIN_SYMBOL, maxSymbol[] = MAX_SYMBOL;
    lcd.writeBitmap(minSymbol, sizeof(minSymbol));
  
    lcd.print(_model->getPressureExtremum(true) / 100.0F, 0);
    lcd.gotoXY(LCD_WIDTH / 2, 5);
    lcd.writeBitmap(maxSymbol, sizeof(maxSymbol));
    lcd.print(_model->getPressureExtremum(false) / 100.0F, 0);
}

bool ViewAltitude::onKeyDown(unsigned char key)
{
  return false;
}
