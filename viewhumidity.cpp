#include "viewhumidity.h"
#include "lib/PCD8544/PCD8544.h"
extern PCD8544 lcd;

ViewHumidity::ViewHumidity()
{

}

void ViewHumidity::draw(bool force)
{
    if(force) {
      lcd.LcdClear();
    }

    lcd.gotoXY(0, 0);
    lcd.print("Td");
    lcd.print(_model->getDewPoint(), 1);

    lcd.plotLine(0,  9, LCD_WIDTH-1, 9);

    lcd.gotoXY(1, 2);
    lcd.setLargeNumbers(true);
    lcd.print(_model->humidity, 1);
    lcd.setLargeNumbers(false);
    lcd.print(" %");

    lcd.plotLine(0, 38 , LCD_WIDTH-1, 38);

    lcd.gotoXY(0, 5);
    lcd.print("Hello world");
}

bool ViewHumidity::onKeyDown(unsigned char key)
{
  return false;
}
