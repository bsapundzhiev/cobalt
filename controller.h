#include "ievents.h"
#include "viewaltitude.h"
#include "viewtemp.h"
#include "viewhumidity.h"
#include "viewsummary.h"
#include "model.h"

class Controller: public IEvents {

private:
    //views
    ViewAltitude _viewAltitude;
    ViewTemp _viewTemp;
    ViewHumidity _viewHumidty;
    ViewSummary _viewSummary;
    IView *_views[4];
    //Models
    BME280_data _model;
    byte _currentViewId;
    bool _displayOn;
    void changeView();
    IView *getCurrentView();
    void dislpayTimeout(bool on = false);
    void switchDisplay();
    void highlightDisplay();
    void beep();
public:
    Controller();
    virtual bool begin();
    virtual void onKeyDown(unsigned char key, unsigned int time);
    virtual void onDataAvailable();
    virtual void onVccMesure(unsigned long vcc);
    void showAlert(char *text, unsigned int timeout = 0);
};
