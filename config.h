#ifndef COBALT_CONFIG_H
#define COBALT_CONFIG_H

#define FW_NAME "C0balt"
#define FW_VERSION_MAJOR 0
#define FW_VERSION_MINOR 1

/*
Hardware: (Note most of these pins can be swapped)
    Graphic LCD Pin ---------- Arduino Pin
       1-VCC       ----------------  5V
       2-GND       ----------------  GND
       3-SCE       ----------------  7
       4-RST       ----------------  6
       5-D/C       ----------------  5
       6-DN(MOSI)  ----------------  11
       7-SCLK      ----------------  13
       8-LED       - 330 Ohm res --  9
*/
#define PIN_SCE     7  // LCD CS  .... Pin 3
#define PIN_RESET   6  // LCD RST .... Pin 1
#define PIN_DC      5  // LCD Dat/Com. Pin 5
#define PIN_SDIN    11 //4  // LCD SPIDat . Pin 6
#define PIN_SCLK    13 //3  // LCD SPIClk . Pin 4
#define PIN_BLIGHT  9  //Back light
//Buttons
#define PIN_BTN_1   4
#define PIN_BTN_2   8
#define PIN_BTN_3   9
#define PIN_BTN_4   10

#define ARR_SIZE(arr) \
  sizeof(arr) / sizeof(arr[0])

#define MIN_SYMBOL      { 0x10, 0x30, 0x7F, 0x30, 0x10 }
#define MAX_SYMBOL      { 0x04, 0x06, 0x7F, 0x06, 0x04 }
#define MIN_SYMBOL1     { 0x10, 0x3F, 0x7F, 0x3F, 0x10 }
#define MAX_SYMBOL1     { 0x04, 0x7E, 0x7F, 0x7E, 0x04 }
#define UP_SYMBOL       { 0x10, 0x18, 0x1C, 0x18, 0x10 }
#define DOWN_SYMBOL     { 0x04, 0x0C, 0x1C, 0x0C, 0x04 }
//
//weather
//
#define STABLE_SYMBOL   { 0x14, 0x16, 0x55, 0x34, 0x14 }
#define SUN_SYMBOL      { 0x2A, 0x1C, 0x3E, 0x1C, 0x2A }
#define RAIN_SYMBOL     { 0x2A, 0x57, 0x2F, 0x57, 0x2A }
#define THUNDER_SYMBOL  { 0x00, 0x0E, 0x1C, 0x38, 0x00 }
#define CLOUD_SYMBOL    { 0x08, 0x1C, 0x1C, 0x1C, 0x08 }

#endif
