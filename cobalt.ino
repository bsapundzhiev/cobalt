/*
 * Author: Borislav Sapundzhiev
 * License: Officially, the MIT License. Review the included License.md file
 * Unofficially, Beerware. Feel free to use, reuse, and modify this
 * code as you see fit. If you find it useful, and we meet someday,
 * you can buy me a beer.
 */


#include "config.h"
#include <Arduino.h>
#include "lib/PCD8544/PCD8544.h"
#include "buttons.h"
#include "clocknortc.h"
//Ctrls
#include "controller.h"

PCD8544 lcd(PIN_SCE, PIN_RESET,PIN_DC,PIN_SDIN,PIN_SCLK,PIN_BLIGHT);
Buttons btns(PIN_BTN_1, PIN_BTN_2, PIN_BTN_3, PIN_BTN_4);
ClockNoRTC clock;

Controller ctrl;
unsigned long updateTime = 0;

void setup(void)
{
  lcd.begin();
  Serial.begin(9600);
  ctrl.begin();
}

void loop(void)
{

  /*while (Serial.available()) {
    byte inByte = Serial.read();
    Serial.print(byte);
  }*/

  btns.checkButtonState();
  for(byte index=0; index < NUMBUTTONS; index++){
    if(btns.readButton(index)) {
      unsigned int tm = btns.getPressTime(index);
      ctrl.onKeyDown(index, tm);

    }
  }

  if(millis() - updateTime >= MODEL_UPDATE_INTERVAL_MS || updateTime == 0) {
    ctrl.onDataAvailable();
    ctrl.onVccMesure(readVcc());
    updateTime = millis();
  }

  delay(50);
}

/*******************************************
 *
 * Internal battery ADC measuring
 *
 *******************************************/
long readVcc() {
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADcdMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

