#ifdef _DEBUG

#include <iostream>
#include "controller.h"


Controller ctrl;


int main(int argc, char const *argv[])
{


  IEvents *ev = &ctrl;

  std::cout <<"controller: " << ev->avalible() << std::endl;


  char c;

  for(;;) {

    std::cin.get(c);
    ev->onKeyDown(c, 0);
  }

  return 0;
}

#endif
