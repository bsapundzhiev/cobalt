#ifndef __IView_H
#define __IView_H
/*
 * View events
 */
 
class BME280_data;

class IView {

public:
    virtual void draw(bool force = false) = 0;
    virtual bool onKeyDown(unsigned char key) = 0;
    virtual void setModel(BME280_data &model) = 0;
};

#endif
