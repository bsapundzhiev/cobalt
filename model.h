#ifndef __MODEL_H
#define __MODEL_H

#include "config.h"
#include "lib/BME280/Adafruit_BME280.h"
#include "weather.h"

#define SEALEVELPRESSURE_HPA (1013.25)
#define MIN   0
#define MAX   1
#define STAY  0
#define DOWN  1
#define UP    2

#define MODEL_UPDATE_INTERVAL_MIN   1
#define MODEL_UPDATE_INTERVAL_MS   (MODEL_UPDATE_INTERVAL_MIN * 60 * 1000UL)
#define LN log

class BME280_data {

    Adafruit_BME280 bme;
    float altitudeArrary[2];
    float minMaxAltitude[2];
    float minMaxTemperature[2];
    float minMaxPressure[2];
    WeatherStation station;
public:

    float pressure;
    float temperature;
    float humidity;

    float positiveDenivelation;
    float negativeDenivelation;


    BME280_data():
      altitudeArrary { 0.0, 0.0 }
    {}
    ~BME280_data(){}

    bool begin() {

      if (!bme.begin()) {
        return bme.begin(0x76);
      }
      return true;
    }

    bool readData() {
        temperature = bme.readTemperature();
        pressure = bme.readPressure();
        float altitude = bme.readAltitude(SEALEVELPRESSURE_HPA);
        humidity = bme.readHumidity();
        // set station presure
        station.setPressure(pressure / 100.F);
        //alt interpolation
        if(altitudeArrary[1] != altitudeArrary[0]) {
          float alt = lerp(altitudeArrary[0], altitudeArrary[1], altitude);
          if (altitudeArrary[0] - alt > 1.0 &&  altitudeArrary[0] - alt < -1.0) {
            altitudeArrary[1] = altitudeArrary[0];
            altitudeArrary[0] = alt;
          }
        } else {
          altitudeArrary[0] = altitude;
        }

        if(altitudeArrary[1] > 0 && altitudeArrary[1] < altitudeArrary[0]) {
          positiveDenivelation += altitudeArrary[0] - altitudeArrary[1];
        }

        if(altitudeArrary[1] > 0 && altitudeArrary[1] > altitudeArrary[0]) {
          negativeDenivelation += altitudeArrary[0] - altitudeArrary[1];
        }

        //Alt extremum
        if (altitudeArrary[MIN] == 0.0F) {
          altitudeArrary[MIN] = altitudeArrary[0];
        }
        minMaxAltitude[MIN] = min(altitudeArrary[MIN] , altitudeArrary[0]);
        minMaxAltitude[MAX] = max(altitudeArrary[MAX] , altitudeArrary[0]);
        //Temp extremum
        if (minMaxTemperature[MIN] == 0.0F) {
          minMaxTemperature[MIN] = temperature;
        }
        minMaxTemperature[MIN] = min(minMaxTemperature[MIN], temperature);
        minMaxTemperature[MAX] = max(minMaxTemperature[MAX], temperature);

        //Pressure extremum
        if (minMaxPressure[MIN] == 0.0F) {
          minMaxPressure[MIN] = pressure;
        }
        minMaxPressure[MIN]= min(minMaxPressure[MIN], pressure);
        minMaxPressure[MAX]= max(minMaxPressure[MAX], pressure);

        return true;
    }

    float getPressure()
    {
      return pressure;
    }

    float getPressureExtremum(bool min) {

      return min ? minMaxPressure[MIN] : minMaxPressure[MAX];
    }

    float getTemperatureExtremum(bool min) {

      return min ? minMaxTemperature[MIN] : minMaxTemperature[MAX];
    }

    float getAltitude()
    {
      return  altitudeArrary[0];
    }

    float getAltitudeExtremum(bool min)
    {
      return min ? minMaxAltitude[MIN] : minMaxAltitude[MAX];
    }

    float verticalSpeed()
    {
      float result = 0;
      if (altitudeArrary[1] > 0 && altitudeArrary[0] > altitudeArrary[1]) {
          result = ((altitudeArrary[0] - altitudeArrary[1]) / MODEL_UPDATE_INTERVAL_MIN);
      }
      return result;
    }

    byte getMovementDirection()
    {
        if(altitudeArrary[1] > altitudeArrary[0]) {
          return DOWN;
        }

        if(altitudeArrary[1] < altitudeArrary[0]){
          return UP;
        }

        return STAY;
    }

    byte getWeatherForecast()
    {
      return station.getWeatherForecast();
    }

    float lerp(float v0, float v1, float t) {
        return v0 + t*(v1-v0);
    }

    //https://en.wikipedia.org/wiki/Dew_point
    //http://andrew.rsmas.miami.edu/bmcnoldy/Humidity.html
    float getDewPoint()
    {
        float RH = humidity;
        float T = temperature;
        float TD =243.04*(LN(RH/100)+((17.625*T)/(243.04+T)))/(17.625-LN(RH/100)-((17.625*T)/(243.04+T)));
        return TD;
    }
};
#endif
