#ifndef __BUTTONS_H
#define __BUTTONS_H

#define NUMBUTTONS  4
#define LONG_PRESS  1000

#define BTN_1 0
#define BTN_2 1
#define BTN_3 2
#define BTN_4 3

class Buttons
{
    byte buttons[NUMBUTTONS]; 
    byte current[NUMBUTTONS];
    byte previous[NUMBUTTONS];
    unsigned long firstTime[NUMBUTTONS];

public:

    Buttons(byte pin_btn1, byte pin_btn2, byte pin_btn3, byte pin_btn4):
    buttons { pin_btn1, pin_btn2,  pin_btn3,  pin_btn4 }
    { 
        
        for(byte index = 0; index < NUMBUTTONS; index++) {
            pinMode(buttons[index], OUTPUT);
            digitalWrite(buttons[index], HIGH); 
            current[index] = LOW;
            previous[index] = LOW;
        }
    }
    
    /*bool isLongPress(byte index)
    {
        if (firstTime[index] > 0  && (millis() - firstTime[index]) >= LONG_PRESS) {
            firstTime[index] = 0;
            return true;
        } 
        return false;
    }*/
    
    unsigned int getPressTime(byte index) 
    {
        unsigned int current = millis() - firstTime[index];
        firstTime[index] = 0;
        return current;
    }
    
    void checkButtonState()
    {

        for(byte index = 0; index < NUMBUTTONS; index++) {

            current[index] = !digitalRead(buttons[index]);

            if(current[index] != LOW && previous[index] != HIGH) {
               firstTime[index] = millis();
            }
        }
    }

    byte readButton(byte index)
    {
        bool ret = false;
        
        if(current[index] == LOW && previous[index] == HIGH) {
            ret = true;    
        } 
         
        previous[index] = current[index];
        
        return ret;
    }
};

#endif
